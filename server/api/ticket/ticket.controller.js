
'use strict';

import jsonpatch from 'fast-json-patch';
import Ticket from './ticket.model';
import User from '../user/user.model';
import Comment from '../comment/comment.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Tickets
export function index(req, res) {
  return Ticket.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Ticket from the DB
export function show(req, res) {
  return Ticket.findById(req.params.ticketId)
    .populate({path: 'owner', Model: 'User', select: 'firstname lastname email _id'})
    .populate({path: 'assignee', Model: 'User', select: 'firstname lastname email _id'})
    .populate({path: 'comments', Model: 'Comment', populate: { path: 'by', model: 'User', select: 'firstname lastname _id'}})
    .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function getMemberTickets(req, res) {
  if(!req.user) {
    return res.status(401).end();
  }

  if(req.user.role == 'admin' || req.user.role == 'superuser') {
    return Ticket.find().exec()
      .then(respondWithResult(res))
      .catch(handleError(res));
  } else {
    return Ticket.find({$or: [{ owner: req.user._id }, { assignee: req.user._id }]})
    .populate({path: 'assignee', Model: 'User', select: 'firstname lastname email _id'})
    .exec()
      .then(respondWithResult(res))
      .catch(handleError(res));
  }
}
// Creates a new Ticket in the DB
export function create(req, res) {
  if(!req.user) {
    return res.status(401).end();
  }
  req.body.owner = req.user._id;
  return Ticket.create(req.body)
  .then(respondWithResult(res, 201))
  .catch(handleError(res));
}

//create offline tickets
export function createOfflineTickets(req, res) {
  if(!req.user) {
    return res.status(401).end();
  }
  var promises = [];
  for(var i = 0; i < req.body.length; i++) {
    req.body[i].owner = req.user._id;
    var p = createOfflineTicket(req.body[i]);
    promises.push(p);
  }
  return Promise.all(promises).then(values => {
    return res.status(201).json(values);
  })
    .catch(handleError(res));
}
function createOfflineTicket(ticketObj) {
  return new Promise(function(resolve, reject) {
    if(ticketObj._id) {
      Reflect.deleteProperty(ticketObj, '_id');
    }
    var b = Ticket.create(ticketObj);
    return b.then(ticket => {
      ticket.set({
        createdAt: ticketObj.createdAt,
        updatedAt: ticketObj.updatedAt
      }, { raw: true });
      return ticket.save({
        silent: true,
        fields: ['createdAt', 'updatedAt']
      }).then(function(ticketTimeStamp) {
        resolve(ticketTimeStamp);
      });
    }).catch(err => {
      reject(err);
    });
  });
}
// Upserts the given Ticket in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Ticket.findOneAndUpdate({_id: req.params.ticketId}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function updateStatus(req, res) {
  if(!req.user) {
    return res.status(401).end();
  }
  var commentId = req.params.commentId;
  return Ticket.findById(req.params.ticketId).exec()
    .then(function(ticket) {
      ticket.status = req.body.status;
      // ticket.comments.push(commentId);
      ticket.comments = ticket.comments.concat([commentId]); // workaround array.push bug in mongodb 3.6
      ticket.save();
      return res.status(201).json(ticket);
    });
}
export function updateAssignee(req, res) {
  if(!req.user) {
    return res.status(401).end();
  }
  var commentId = req.params.commentId;
  return Ticket.findById(req.params.ticketId).exec()
    .then(function(ticket) {
      ticket.assignee = req.body.assignee;
      ticket.status = req.body.status;
      // ticket.comments.push(commentId);
      ticket.comments = ticket.comments.concat([commentId]); // workaround array.push bug in mongodb 3.6
      ticket.save();
      return res.status(201).json(ticket);
    });
}

export function updateComment(req, res) {
  if(!req.user) {
    return res.status(401).end();
  }
  var commentId = req.params.commentId;
  return Ticket.findById(req.params.ticketId).exec()
    .then(function(ticket) {
      //ticket.comments.push(commentId);
      ticket.comments = ticket.comments.concat([commentId]); // workaround array.push bug in mongodb 3.6
      ticket.save();
      return res.status(201).json(ticket);
    });
}

// Updates an existing Ticket in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Ticket.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Ticket from the DB
export function destroy(req, res) {
  return Ticket.findById(req.params.ticketId).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
