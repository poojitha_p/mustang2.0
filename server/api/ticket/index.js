'use strict';

var express = require('express');
var controller = require('./ticket.controller');
import * as auth from '../../auth/auth.service';
var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/memberof', auth.isAuthenticated(), controller.getMemberTickets);
router.get('/:ticketId', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.post('/offline', auth.isAuthenticated(), controller.createOfflineTickets);
router.put('/:ticketId', auth.isAuthenticated(), controller.upsert);
router.put('/:ticketId/assignee/:commentId', auth.hasRole('superuser'), controller.updateAssignee);
router.put('/:ticketId/status/:commentId', auth.isAuthenticated(), controller.updateStatus);
router.put('/:ticketId/comment/:commentId', auth.isAuthenticated(), controller.updateComment);
router.delete('/:ticketId', auth.hasRole('superuser'), controller.destroy);

module.exports = router;
