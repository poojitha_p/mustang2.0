'use strict';

import mongoose from 'mongoose';
import User from '../user/user.model';
import Comment from '../comment/comment.model';

var TicketSchema = new mongoose.Schema({
  subject: String,
  description: String,
  status: {
    type: String,
    enum: ['new', 'assigned', 'Open', 'Resolved', 'On hold', 'Duplicate', 'WIP', 'Invalid', 'Wont fix', 'Closed']
  },
  type: String,
  owner: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  assignee: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  comments: [{
    type: mongoose.Schema.ObjectId,
    ref: 'Comment'
  },
    {
      usePushEach: true
    }]
},
  {
    timestamps: true
  });

export default mongoose.model('Ticket', TicketSchema);
