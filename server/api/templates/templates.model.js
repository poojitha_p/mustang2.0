'use strict';

import mongoose from 'mongoose';

var TemplateSchema = new mongoose.Schema({
  templatename: String,
  tid: String,
  description: String,
  params:[{
    name: String,
    value: String
  }]

});

export default mongoose.model('template', TemplateSchema);
