/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/pawns              ->  index
 * POST    /api/pawns              ->  create
 * GET     /api/pawns/:name          ->  show
 * PUT     /api/pawns/:name          ->  upsert
 * PATCH   /api/pawns/:name          ->  patch
 * DELETE  /api/pawns/:name          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Employee from './emp.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of Employees
export function index(req, res) {
  return Employee.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Pawn from the DB
export function show(req, res) {
  return Employee.findById(req.params.id)
  .populate({path:'departments', Model:'departments', select:'name _id'})
  .exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a single Pawn by name from the DB
export function getByName(req, res) {
  return Employee.find({name: req.params.name}).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}
// Creates a new Pawn in the DB
export function create(req, res) {
  req.body.user = req.user;
  return Employee.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given Pawn in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Employee.findOneAndUpdate({_id: req.params.id}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Pawn in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Employee.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Pawn from the DB
export function destroy(req, res) {
  return Employee.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
