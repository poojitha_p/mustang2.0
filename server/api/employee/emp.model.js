'use strict';

import mongoose from 'mongoose';
import User from '../user/user.model';
import departments from '../departments/departments.model'
var EmployeeSchema = new mongoose.Schema({
  empId:String,
  userId: {
    type: mongoose.Schema.ObjectId,
    ref : 'User'
  },
  joiningdate:Date,
  fname: String,
  mname: String,
  lname: String,
  fathername:String,
  primaryemail: String,
  secondaryemail: String,
  mobile: Number,
  mobilenumber: Number,
  dob:Date,
  genderMale:String,
  genderFemale:String,
  genderTransgender:String,
  empId:String,
  panId: String,
  aadhar: Number,
  passport: String,
  validfrom:Date,
  validto:Date,
  nationality:String,
  aadhar:Number,
  statusSingle:String,
  statusMarried:String,
  statusWidow:String,
  statusDivorce:String,
  SpouseName:String,
  CurrentAddress:{
    type:String,
  },
  Pos:{
      type:Date
    },
  PermanentAddress:{
    type:String,
  },
    pos:{
      type:Date
    },
  designation:{
    type: String,
    // enum: ['']
  },
  departments:{ /// add reference
    type: mongoose.Schema.ObjectId,
    ref: 'departments'
  },

  education:{
    gap:Boolean,
    Howmanyyears:String,
    tenth: {
      tenth:Boolean,
      Insname: String,
      tenthpassedout: {
        type: Date
      },
      tenthpercentage: Number
    },
    intermediate: {
      intermediate:Boolean,
      insname: String,
      Specialization:String,
      Interpassedout: {
        type: Date
      },
      Intermediatepercentage: Number
    },
    graduation: {
      graduation:Boolean,
      Uniname: String,
      gradspeci:String,
      From: {
        type: Date
      },
      To: {
        type: Date
      },
      graduationpercentage: Number
    },
    postgraduation:{
      postgraduation:Boolean,
      uniname: String,
      postspeci:String,
      pgfrom: {
        type: Date
      },
      pgto: {
        type: Date
      },
      postgraduationpercentage: Number
    },
    pHD: {
      pHD:Boolean,
      universityname: String,
      pHDspec: String,
      pHDfrom: {
        type: Date
      },
      pHDto: {
        type: Date
      },
      pHDpercentage: Number
    }


  },
  employment: {

      Experience:Number,
      Fresher:Boolean,
      companyname: String,
      Fromdate: {
        type: Date
      },
      Todate: {
        type: Date
      },
      designations: String
    }

});

export default mongoose.model('Employee', EmployeeSchema);
