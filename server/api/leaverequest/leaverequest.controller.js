/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/leaverequest              ->  index
 * POST    /api/leaverequest              ->  create
 * GET     /api/leaverequest/:name          ->  show
 * PUT     /api/leaverequest/:name          ->  upsert
 * PATCH   /api/leaverequest/:name          ->  patch
 * DELETE  /api/leaverequest/:name          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Leaverequest from './leaverequest.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of leaverequest
export function index(req, res) {
  return Leaverequest.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single leaverequest from the DB
export function show(req, res) {
  return Leaverequest.findById(req.params.leaverequestId).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a single leaverequest by name from the DB
export function getByName(req, res) {
  return Leaverequest.find({name: req.params.name}).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}
// Creates a new leaverequest in the DB
export function create(req, res) {
  return Leaverequest.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given leaverequest in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Leaverequest.findOneAndUpdate({_id: req.params.leaverequestId}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing leaverequest in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Leaverequest.findById(req.params.leaverequestId).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a leaverequest from the DB
export function destroy(req, res) {
  return Leaverequest.findById(req.params.leaverequestId).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
