'use strict';

import mongoose from 'mongoose';
import Employee from '../employee/emp.model';


var LeaverequestSchema = new mongoose.Schema({
  empname: String,
  empId: [{
    type : mongoose.Schema.ObjectId,
    ref : 'Employee'
  }],
  department: String,
  leavetype: String,
  employeestatus: String,
  leavestatus: String,
  from: Date,
  to: Date,
  leavemessage: String,
  leaveAccrueEnabled: String,
  leaveCarriedForward: String,
  leavePerYear: Number,
  noofdays: Number,
  approvedby: String,
  // leavetype {
  //   Vacationleaves: Number,
  //   Annualleaves: Number,
  //   Sickleaves: Number,
  //   Casualleaves: Number,
  //   Earnedleaves/pl: Number,
  //   Maternityleaves: Number,
  //   Paternityleaves: Number,
  //   Transferleaves: Number
  // }
});

export default mongoose.model('Leaverequest', LeaverequestSchema);
