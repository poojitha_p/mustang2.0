'use strict';

var express = require('express');
var controller = require('./leaverequest.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:leaverequestId', controller.show);
// router.get('/:name', controller.getByName);
router.post('/', controller.create);
router.put('/:leaverequestId', controller.upsert);
router.patch('/:leaverequestId', controller.patch);
router.delete('/:leaverequestId', controller.destroy);

module.exports = router;
