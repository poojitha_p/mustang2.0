'use strict';

var express = require('express');
var controller = require('./departments.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:departmentsId', controller.show);
// router.get('/departments:name', controller.getByName);
router.post('/', controller.create);
router.put('/:departmentsId', controller.upsert);
router.patch('/:departmentsId', controller.patch);
router.delete('/:departmentsId', controller.destroy);

module.exports = router;
