'use strict';

import mongoose from 'mongoose';

var departmentsSchema = new mongoose.Schema({
  name: String,
  id: Number,
  summary:String
  });

export default mongoose.model('departments', departmentsSchema);
