'use strict';

import mongoose from 'mongoose';

var JobSchema = new mongoose.Schema({
  title: String,
  description: String,
  keyword: String,
  cn:String,
  annual: Number,
  type: String,
  nov:Number,
  np:Number,
  experience: String,
  industry: String,
  area: String,
  location: String,
  role:String


});

export default mongoose.model('Job', JobSchema);
