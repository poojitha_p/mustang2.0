'use strict';

var express = require('express');
var controller = require('./requirements.controller');

var router = express.Router();

router.get('/', controller.getRequirements);
router.get('/besilly', controller.greet);
router.get('/modules', controller.getModules);
router.post('/modules', controller.addModule);


module.exports = router;
