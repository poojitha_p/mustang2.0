/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/things              ->  index
 * POST    /api/things              ->  create
 * GET     /api/things/:id          ->  show
 * PUT     /api/things/:id          ->  upsert
 * PATCH   /api/things/:id          ->  patch
 * DELETE  /api/things/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Module from './module.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log('err:' +err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of requirements
export function getRequirements(req, res) {
  //return res.status(200).send(' You are going to see something here soon.');
  var dummyResponse = {
    status: 'OK',
    message: ' You are going to see something here soon.'
  };
  return res.status(200).json(dummyResponse);
}

// Gets a list of modules
export function getModules(req, res) {
  //return res.status(200).send(' You are going to see something here soon.');
  // var modules = [
  //   {
  //     "name": "PIM",
  //     "id": "pim",
  //     "summary": "Personal Info Management"
  //   },
  //   {
  //     "name": "Admin",
  //     "id": "admin",
  //     "summary": "Administrator Stuff"
  //   },
  //   {
  //     "name": "Attendance",
  //     "id": "attendance",
  //     "summary": "Attendance and Leaves"
  //   },
  //   {
  //     "name": "Holidays",
  //     "id": "holidays",
  //     "summary": "Yes, all are on weekends."
  //   },
  //   {
  //     "name": "Time",
  //     "id": "time",
  //     "summary": "Tick tock tick tock"
  //   },
  //   {
  //     "name": "Recruitment",
  //     "id": "recruitment",
  //     "summary": "Hiring and hiring and hiring"
  //   }
  // ];

  // Gets a list of modules
  return Module.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));

}

// Gets a silly msg
export function greet(req, res) {
  return res.status(200).send(' You are falling asleep in the next 5 seconds.');
}

// Creates a new module in the DB
export function addModule(req, res) {
  console.log(' adding a module' +  req.body);
  return Module.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}
