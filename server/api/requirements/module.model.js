'use strict';

import mongoose from 'mongoose';

var ModuleSchema = new mongoose.Schema({
  name: {
    type:String,
    required: true
  },
  moduleId: String,
  summary: String
});

export default mongoose.model('Module', ModuleSchema);
