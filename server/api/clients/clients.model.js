'use strict';

import mongoose from 'mongoose';

var ClientSchema = new mongoose.Schema({
  name: String,
  id: String ,
  description: String,
  weburl: String,
  companysize: String,
  industry: String,
  year: Number,
  companytype: String,
  location: String
});

export default mongoose.model('Client', ClientSchema);
