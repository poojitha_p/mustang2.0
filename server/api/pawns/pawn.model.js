'use strict';

import mongoose from 'mongoose';

var PawnSchema = new mongoose.Schema({
  name: {
    type:String,
    enum: ['king', 'queen', 'rook', 'knight', 'bishop', 'pawn'],
    default: 'pawn',
    required: true,
    lowercase: true
  },
  color: {
    type: String,
    enum: ['white', 'black'],
    default: 'white'
  },
  summary: String
});

export default mongoose.model('Pawn', PawnSchema);
