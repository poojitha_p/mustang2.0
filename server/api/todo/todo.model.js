'use strict';

import mongoose from 'mongoose';
import User from '../user/user.model';

var TodoSchema = new mongoose.Schema({
  title: {
    type:String,
    required: true
  },
  id: String,
  summary: String,
  owner: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  status:{
    type: String,
    enum: ['in-que', 'done', 'overdue'],
    default: 'in-que'
  }
});

export default mongoose.model('Todo', TodoSchema);
