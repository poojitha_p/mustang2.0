/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/payroll              ->  index
 * POST    /api/payroll              ->  create
 * GET     /api/payroll/:name          ->  show
 * PUT     /api/payroll/:name          ->  upsert
 * PATCH   /api/payroll/:name          ->  patch
 * DELETE  /api/payroll/:name          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Payroll from './payroll.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of payroll
export function index(req, res) {
  return Payroll.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single payroll from the DB
export function show(req, res) {
  return Payroll.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a single payroll by name from the DB
export function getByName(req, res) {
  return Payroll.find({name: req.params.name}).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}
// Creates a new payroll in the DB
export function create(req, res) {
  return Payroll.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given payroll in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Payroll.findOneAndUpdate({_id: req.params.payrollId}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing payroll in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Payroll.findById(req.params.payrollId).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a payroll from the DB
export function destroy(req, res) {
  return Payroll.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
