'use strict';

var express = require('express');
var controller = require('./payroll.controller');

var router = express.Router();
// module.exports = router;
router.get('/', controller.index);
// router.get('/:name', controller.getByName);
router.post('/', controller.create);
router.get('/:id', controller.show);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);

module.exports = router;
