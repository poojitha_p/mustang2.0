'use strict';

import mongoose from 'mongoose';

var payrollSchema = new mongoose.Schema({
  Ctc: Number,
  Variable: Number,
  Hra:Number,
  Pf: Number,
  Basicallowance: Number,
  Pt: Number,
  Travelallowance: Number,
  Emi:Number,
  Medicalallowance: Number,
  Esi: Number,
  It: Number,
  Subtotal: Number,
  Total: Number,
  Grosstotal: Number
  });

export default mongoose.model('payroll', payrollSchema);
