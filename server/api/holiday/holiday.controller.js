/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/s              ->  index
 * POST    /api/holidayss              ->  create
 * GET     /api/holidayss/:name          ->  show
 * PUT     /api/holidayss/:name          ->  upsert
 * PATCH   /api/holidayss/:name          ->  patch
 * DELETE  /api/holidayss/:name          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Holiday from './holiday.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    console.log(err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of holidayss
export function index(req, res) {
  return Holiday.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single holidays from the DB
export function show(req, res) {
  return Holiday.findById(req.params.holidayId).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}
  export function holidaylist (req, res) {
    var data = [
     // { date : "01/26/2018",
     //   name : "Republic Day",
     //   summary: "National holiday"
     //
     // },
     // { date : "08/15/2018",
     //   name : "Independence Day",
     //   summary: "National holiday"
     //
     // },
     // { date : "09/13/2018",
     //   name : "Ganesh Chathurti",
     //   summary: "Festival"
     //
     // },
     // { date : "10/02/2018",
     //   name : "Mahatma Gandhi Jayanthi",
     //   summary: "National holiday",
     //   summary: "Festival"
     //
     // },
     // { date : "10/19/2018",
     //   name : "Dussehra Festival",
     //   summary: "Festival"
     //
     // },
     // {   date :   "11/07/2018",
     //     name : "Diwali Festival",
     //     summary: "Festival"
     //
     // },
     // {   date : "11/21/2018",
     //     name : "Milad un-Nabi",
     //     summary: "Festival"
     // },
     // { date : "12/25/2018",
     //   name : "Christmas ",
     //   summary: "Festival"
     //
     // },
     //
     // { date : "01/26/2018",
     //   name : "Republic Day",
     //  summary: "National holiday"
     // },
     // { date : "01/01/2019",
     //   name : "New Year",
     //  summary: "National holiday"
     //
     // },
     // { date : "01/14/2018",
     //   name : "Makar Sankranthi",
     //   summary: "Festival"
     // },
     // { date : "01/15/2019",
     //   name : "Pongal",
     //   summary: "Festival"
     // }
];
return res.status(200).json(data);
// .catch(handleError(res));
}




// Creates a new holidays in the DB
export function create(req, res) {
  return Holiday.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Upserts the given holidays in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Holiday.findOneAndUpdate({_id: req.params.holidayId}, req.body, {new: true, upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing holidays in the DB
export function patch(req, res) {
  if(req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Holiday.findById(req.params.holidayId).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a holidays from the DB
export function destroy(req, res) {
  return Holiday.findById(req.params.holidayId).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
