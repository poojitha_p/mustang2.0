'use strict';

import mongoose from 'mongoose';

var HolidaySchema = new mongoose.Schema({
  title: String,
  date: Date,
 summary: String,

});

export default mongoose.model('Holiday', HolidaySchema);
