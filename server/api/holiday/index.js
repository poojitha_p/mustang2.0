'use strict';

var express = require('express');
var controller = require('./holiday.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:holidayId', controller.show);
router.get('/holidays/list', controller.holidaylist);
router.post('/', controller.create);
router.put('/:holidayId', controller.upsert);
router.patch('/:holidayId', controller.patch);
router.delete('/:holidayId', controller.destroy);

module.exports = router;
