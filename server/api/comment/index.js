'use strict';

var express = require('express');
var controller = require('./comment.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.hasRole('user'), controller.index);
router.get('/:commentId', auth.hasRole('user'), controller.show);
router.post('/', auth.hasRole('user'), controller.addcomment);
router.put('/:commentId', auth.hasRole('user'), controller.updatecomment);
router.delete('/:commentId', auth.hasRole('user'), controller.deletecomment);
router.delete('/:contractId/:commentId', auth.hasRole('user'), controller.deleteCommentContract);

router.put('/:commentId/reply', auth.hasRole('user'), controller.replyComment);


module.exports = router;
