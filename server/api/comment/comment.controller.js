
'use strict';

import Comment from './comment.model';
// import Contract from '../contract/contract.model';
// import Requirement from '../requirement/requirement.model';
// import Task from '../task/task.model';


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;

  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Comments
export function index(req, res) {
  return Comment.find()
  .populate({path:'by', Model:'User', select:'firstname lastname _id'})
  .exec()
  .then(respondWithResult(res))
  .catch(handleError(res));

}

// Gets a single Comment from the DB
export function show(req, res) {
  return Comment.findById(req.params.commentId)
  .populate({path:'by', Model:'User', select:'firstname lastname image _id'})
  .populate({path:'replies', Model:'Comment', populate: { path: 'by', model: 'User', select:'firstname  lastname image _id'}})
  .exec()
  .then(handleEntityNotFound(res))
  .then(respondWithResult(res))
  .catch(handleError(res));
}

// Creates a new Comment in the DB
export function addcomment(req, res) {

  if(!req.user) {
    return res.status(401).end();
  }

  req.body.by = req.user._id;
  return Comment.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

//reply comment

export function  replyComment(req, res){
  if(!req.user) {
    return res.status(401).end();
  }
  var commentId = req.params.commentId;
  req.body.by = req.user._id;
  return Comment.findById(req.params.commentId).exec()
    .then(comment => {
      Comment.create(req.body)
        .then(c =>{
          //comment.replies.push(c);
          comment.replies = comment.replies.concat([c]); // workaround array.push bug in mongodb 3.6
          comment.save();
          return res.status(201).json(comment);
        });
    });
}


// Upserts the given Comment in the DB at the specified ID
export function updatecomment(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Comment.findOneAndUpdate({_id: req.params.commentId}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()

    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Comment in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Comment.findById(req.params.commentId).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Comment from the DB
export function deletecomment(req, res) {
  return Comment.findById(req.params.commentId).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

// Deletes a comment using contract from the DB
export function deleteCommentContract(req, res, next) {
  return Comment.find().remove({_id:req.params.commentId}).exec()
     .then(handleEntityNotFound(res))
     .then(respondWithResult(res,204))
       .catch(handleError(res));
     }
