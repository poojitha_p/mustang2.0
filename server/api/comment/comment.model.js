'use strict';

import mongoose from 'mongoose';
import User from '../user/user.model';
// import Issue from '../issue/issue.model';
// import requirement from '../requirement/requirement.model';


var CommentSchema = new mongoose.Schema({
  comment: String,
  by: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  replies: [{
    type: mongoose.Schema.ObjectId,
    ref: 'Comment'
  }],
  status: {
    type: String,
    enum: ['draft', 'created', 'submitted', 'pending-approval', 'approved', 'rejected', 'completed', 'updated', 'request-more-info', 'order-generated', 'cancelled', 'new', 'assigned', 'Open', 'Resolved', 'On hold', 'Duplicate', 'WIP', 'Invalid', 'Wont fix', 'Closed']
  },

},
  {
    timestamps: true
  }
);

var autoPopulateReplies = function(next) {
  this.populate('replies');
  next();
};

var autoPopulateUsers = function(next) {
  this.populate('by', ['firstname', 'lastname', 'email', 'image', '_id'], User);
  next();
};


CommentSchema
.pre('findOne', autoPopulateReplies)
.pre('find', autoPopulateReplies)
.pre('findById', autoPopulateReplies);

CommentSchema
.pre('findOne', autoPopulateUsers)
.pre('find', autoPopulateUsers)
.pre('findById', autoPopulateUsers);

export default mongoose.model('Comment', CommentSchema);
