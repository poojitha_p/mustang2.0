/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/tickets', require('./api/ticket'));
  app.use('/api/comments', require('./api/comment'));
  app.use('/api/employees', require('./api/employee'));
  app.use('/api/requirements', require('./api/requirements'));
  app.use('/api/todos', require('./api/todo'));
  app.use('/api/pawns', require('./api/pawns'));
  app.use('/api/clients', require('./api/clients'));
  app.use('/api/jobs', require('./api/jobs'));
  // app.use('/api/timesheets', require('./api/timesheets'));
  app.use('/api/holiday', require('./api/holiday'));
  app.use('/api/templates', require('./api/templates'));
  app.use('/api/departments', require('./api/departments'));
  app.use('/api/payroll', require('./api/payroll'));
  app.use('/api/leaverequests', require('./api/leaverequest'));
  app.use('/auth', require('./auth').default);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the app.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/app.html`));
    });
}
