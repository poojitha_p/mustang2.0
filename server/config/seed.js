/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import Pawn from '../api/pawns/pawn.model';

import config from './environment/';

export default function seedDatabaseIfNeeded() {
  if(config.seedDB) {
    Thing.find({}).remove()
      .then(() => {
        let thing = Thing.create({
          name: 'Development Tools',
          info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, '
                + 'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, '
                + 'Stylus, Sass, and Less.'
        }, {
          name: 'Server and Client integration',
          info: 'Built with a powerful and fun stack: MongoDB, Express, '
                + 'AngularJS, and Node.'
        }, {
          name: 'Smart Build System',
          info: 'Build system ignores `spec` files, allowing you to keep '
                + 'tests alongside code. Automatic injection of scripts and '
                + 'styles into your app.html'
        }, {
          name: 'Modular Structure',
          info: 'Best practice client and server structures allow for more '
                + 'code reusability and maximum scalability'
        }, {
          name: 'Optimized Build',
          info: 'Build process packs up your templates as a single JavaScript '
                + 'payload, minifies your scripts/css/images, and rewrites asset '
                + 'names for caching.'
        }, {
          name: 'Deployment Ready',
          info: 'Easily deploy your app to Heroku or Openshift with the heroku '
                + 'and openshift subgenerators'
        });
        return thing;
      })
      .then(() => console.log('finished populating things'))
      .catch(err => console.log('error populating things', err));

    // Running this part will mean your dev instance will have all users DELETED and created again.
    // Run once and comment this part if you need your dev instance retain data mapped to users table.
    User.find({}).remove()
      .then(() => {
        User.create(
        {
          provider: 'local',
          role: 'admin',
          name: 'superuser',
          email: 'superuser@niranta.in',
          password: 'admin'
        },
        {
          provider: 'local',
          role: 'admin',
          name: 'bharath chetty',
          email: 'bharath.chetty@niranta.in',
          password: '7b0101'
        },
        {
          provider: 'local',
          role: 'user',
          name: 'poojitha',
          email: 'poojitha@niranta.in',
          password: 'patrapati'
        },
        {
          provider: 'local',
          role: 'admin',
          name: 'sainath',
          email: 'sainath@niranta.in',
          password: 'middi'
        },
        {
          provider: 'local',
          role: 'user',
          name: 'superuser',
          email: 'khanbhai@niranta.in',
          password: 'khanbhai'
        },
        {
          provider: 'local',
          role: 'user',
          name: 'rpa guy',
          email: 'rpaguy@niranta.in',
          password: 'rpaguy'
        },
        {
          provider: 'local',
          role: 'user',
          name: 'second bench',
          email: 'secondbench@niranta.in',
          password: 'secondbench'
        }
      )
        .then(() => console.log('finished populating users'))
        .catch(err => console.log('error populating users', err));
      });


    // Add a couple of pawns
    Pawn.find({}).remove()
      .then(() => {
        let pawn = Pawn.create({
          name: 'King',
        }, {
          name: 'Queen',
        }, {
          name: 'Knight',
        }, {
          name: 'Rook',
        }, {
          name: 'Bishop',
        }, {
          name: 'Pawn',
        });
        return pawn;
      })
      .then(() => console.log('finished populating pawns'))
      .catch(err => console.log('error populating pawns', err));
  }
}
