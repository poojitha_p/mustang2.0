'use strict';
/* eslint no-sync: 0 */

import angular from 'angular';

export class NavbarComponent {
  menu = [
    {
    title: 'Home',
    state: 'main',
    loggedIn: false
    },
    {
    title: 'Dashboard',
    state: 'dashboard',
    loggedIn: true
    },
    {
    title: 'Departments',
    state: 'departments.list',
    loggedIn: true
  },
    {
      title:'Employees',
      state:'employees.list',
      loggedIn:true
    },
    {
      title:'Payroll',
      state:'payroll.list',
      loggedIn:true
    },
    {
      title:'Leave Request',
      state:'leaverequest.list',
      loggedIn:true
    },
    {
    title: 'Clients',
    state: 'clients.list',
    loggedIn: true
  },
    {
    title: 'Jobs',
    state: 'jobs.list',
    loggedIn: true
  },
  {
  title: 'Timesheets',
  state: 'timesheets.list',
  loggedIn: true
  },
  {
    title: 'Holidays',
    state: 'holiday.list',
    loggedIn: true
  },
  {
    title: 'Templates',
    state: 'templates.new',
    loggedIn: true
  }

  ];
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  isCollapsed = true;

  constructor(Auth) {
    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

}

export default angular.module('directives.navbar', [])
  .component('navbar', {
    template: require('./navbar.html'),
    controller: NavbarComponent
  })
  .name;
