'use strict';

import angular from 'angular';

export function Modal($rootScope, $uibModal) {
    'ngInject';
  /**
   * Opens a modal
   * @param  {Object} scope      - an object to be merged with modal's scope
   * @param  {String} modalClass - (optional) class(es) to be applied to the modal
   * @return {Object}            - the instance $uibModal.open() returns
   */
  function openModal(scope = {}, modalClass = 'modal-default') {
    var modalScope = $rootScope.$new();

    angular.extend(modalScope, scope);

    return $uibModal.open({
      template: require('./modal.html'),
      windowClass: modalClass,
      scope: modalScope
    });
  }
  function modalAlertfunc(scope = {}, modalClass = 'modal-default') {
    var modalScope = $rootScope.$new();

    angular.extend(modalScope, scope);

    return $uibModal.open({
      template: require('./modalAlert.html'),
      windowClass: modalClass,
      scope: modalScope
    });
  }

  // Public API here
  return {

    /* Confirmation modals */
    confirm: {
      modalAlert(response, name) {

         var type;
         var html;
         var title;


         switch(response.status){

          case 200:
            if(response.config.method=='PUT'){
              type="alert alert-success",
              html='<h4><strong>' + name +  '</strong>  <span>  updated.</span></h4>',
              title="Success";
            }else{
              if(response.config.method=='DELETE'){
                type="alert alert-success",
                html='<h4><strong>' + name +  '</strong>  <span>  deleted.</span></h4>',
                title="Success";
              }else{
                type="alert alert-success",
                html='<h4><strong>' + name +  '</strong>  <span>  created.</span></h4>',
                title="Success";
              }
            }
            break;
          case 201:
            if(response.config.method=='PUT'){
              type="alert alert-success",
              html='<h4><strong>' + name +  '</strong>  <span>  updated.</span></h4>',
              title="Success";
            }else{
              if(response.config.method=='DELETE'){
                type="alert alert-success",
                html='<h4><strong>' + name +  '</strong>  <span>  deleted.</span></h4>',
                title="Success";
              }else{
                type="alert alert-success",
                html='<h4><strong>' + name +  '</strong>  <span>  created.</span></h4>',
                title="Success";
                }
            }
              break;
          case 204:
            type="alert alert-success",
            html='<h4><strong>' + name +  '</strong>  <span>  deleted.</span></h4>',
            title="Success";
            break;
            case 400:
              type="alert alert-warning",
              html='<h4>Bad request</h4>',
              title="Warning";
              break;
            case 401:
              type="alert alert-warning",
              html='<h4>unauthorized</h4>',
              title="Warning";
              break;
            case 403:
              type="alert alert-warning",
              html='<h4>forbidden</h4>',
              title="Warning";
              break;
            case 404:
              type="alert alert-warning",
              html='<h4>Not found</h4>',
              title="Warning";
              break;
            case 405:
              type="alert alert-warning",
              html='<h4>Method Not allowed</h4>',
              title="Warning";
              break;
            case 409:
                type="alert alert-warning",
                html='<h4>Mail id already exists</h4>',
                title="Warning";
                break;
            case 11000:
              type="alert alert-warning",
              html='<h4><strong>' + name +  '</strong>  <span>  already exists.</span></h4>',
              title="Warning";
              break;
            case 500:
              type="alert alert-warning",
              html='<h4>Internal server error</h4>',
              title="Warning";
              break;
            }
            if(name=="EmailAlert") {
              type="alert alert-success",
              html='<h4>Verify your email address to complete the registration process.</h4>',
              title="Success";
            }else{
              if(name == "jobapply") {
                type="alert alert-success",
                html='<h4>You have successfully applied to this job.</h4>',
                title="Success";
              }
              if(name == "ContactMessage") {
                type="alert alert-success",
                html='<h4>Your message has been sent.</h4>',
                title="Success";
              }
              if(name == "jd") {
                type="alert alert-success",
                html='<h4>Job description has been sent to your email.</h4>',
                title="Success";
              }
              if(name == 'ticketassignee') {
              type="alert alert-success",
              html='<div class="panel-body"><h4>Ticket assigned to <strong>' + response.firstname +  '</strong>.</h4></div>';
              title="Success";
            }
              if(name == "verifyEmail"){
                type="alert alert-warning",
                html='<h4>Looks like your email address is not verified yet !?. Verification email has been re-sent.</h4>',
                title="Warning";
              }
              if(name == 'emailpassword'){
                type="alert alert-success",
                html='<h4>Your password is updated now.</h4>',
                title="Success";
              }
              if(name == 'interview'){
                type="alert alert-success",
                html='<h4>Interview scheduled.</h4>',
                title="Success";
              }
              if(name == 'forgotpassword'){
                type="alert alert-info",
                html='<h4> Check your email for a link to complete resetting your password.</h4>',
                title="Info";
              }
              if(name == 'Email doesnt exist'){
                type="alert alert-warning",
                html='<h4><span>There is no account associated with the email address given. Would you like to <a href="signup">sign up?</a></span></h4>',
                title="Warning";
              }
              // if(response.data.code =="11000"){
              //   type="alert alert-warning",
              //   html='<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><h4 class="fontsize"><i class="mdi mdi-check mdi-48"></i>&nbsp;&nbsp;Warning</h4><hr><h4><strong>' + name +  '</strong>  <span>  already exists.</span></h4></div>';
              // }
            if(response.data =="You have already applied for this job."){
              type="alert alert-warning",
              html='<h4><span>You have already applied for this job.</span></h4></div>',
              title="Warning";
            }

            if(name =="The specified email address is already in use."){
              type="alert alert-warning",
              html='<h4><span>Looks like the email address is already in use. Try <a class= "btn btn-link btn-lg" href="login">Forgot Password</a> from the login page.</span></h4>',
              title="Warning";
            }
            if(name =="Bad Request"){
              type="alert alert-warning",
              html='<h4><span>Bad Request</span></h4>',
              title="Warning";
            }

          }

          switch (type) {
            case  'alert alert-success' :
               var modal ="modal-success";
              break;
              case 'alert alert-warning' :
                var modal ="modal-warning";
                break;

            default:
             var modal = "modal-info";

          }

        var modalAlert = modalAlertfunc({
          modalAlert: {
            dismissable: true,

            html: html,
            type:type,
            title:title,
            click(e) {
                modalAlert.close(e);
            }
          }
        }, modal);

      },
      modalSocketEmit(name, user, activitytype, activityOn) {

         var notification = {};
         notification['name'] = name;
         notification['by'] = user;
         notification['activitytype'] = activitytype;
         notification['activityOn'] = activityOn;

         socket.emit('send message', notification);

      },
      modalProjectSocketEmit(emitName, object) {
         socket.emit(emitName, object);
      },
      /**
       * Create a function to open a delete confirmation modal (ex. ng-click='myModalFn(name, arg1, arg2...)')
       * @param  {Function} del - callback, ran when delete is confirmed
       * @return {Function}     - the function to open the modal (ex. myModalFn)
       */
      delete(del = angular.noop) {
        /**
         * Open a delete confirmation modal
         * @param  {String} name   - name or info to show on modal
         * @param  {All}           - any additional args are passed straight to del callback
         */
        return function() {
          var slicedArgs = Array.prototype.slice.call(arguments);
          var name = slicedArgs.shift();
          var deleteModal;

          deleteModal = openModal({
            modal: {
              dismissable: true,
              title: 'Confirm Delete',
              html: `<p>Are you sure you want to delete <strong>${name}</strong> ?</p>`,
              buttons: [{
                classes: 'btn-danger',
                text: 'Delete',
                click(e) {
                  deleteModal.close(e);
                }
              }, {
                classes: 'btn-default',
                text: 'Cancel',
                click(e) {
                  deleteModal.dismiss(e);
                }
              }]
            }
          }, 'modal-danger');

          deleteModal.result.then(function(event) {
            Reflect.apply(del, event, slicedArgs);
          });
        };
      }
    }
    // timer : {
    //   on(timer = angular.noop) {
    //     /**
    //      * Open a delete confirmation modal
    //      * @param  {String} counter   - name or info to show on modal
    //      * @param  {All}           - any additional args are passed straight to del callback
    //      */
    //     return function(...args) {
    //       var slicedArgs = Reflect.apply(Array.prototype.slice, args);
    //       var counter = slicedArgs.shift();
    //       var timerModal;
    //
    //       timerModal = openModal({
    //         modal: {
    //           dismissable: true,
    //           title: 'Warning',
    //           html: `<p class="text-center">This machine will self-destruct in <strong>${counter}</strong> seconds?</p>`,
    //           buttons: [{
    //             classes: 'btn-danger',
    //             text: 'Delete',
    //             click(e) {
    //               timerModal.close(e);
    //             }
    //           }, {
    //             classes: 'btn-default',
    //             text: 'Cancel',
    //             click(e) {
    //               timerModal.dismiss(e);
    //             }
    //           }]
    //         }
    //       }, 'modal-danger');
    //
    //       timerModal.result.then(function(event) {
    //         Reflect.apply(timer, event, slicedArgs);
    //       });
    //     };
    //   }
    // }

  };
}

// export default angular.module('mustangApp.Modal', [])
//   .factory('Modal', Modal)
//   .name;
