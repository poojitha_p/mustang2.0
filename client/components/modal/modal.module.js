'use strict';

import angular from 'angular';
import {
  Modal
} from './modal.service';

export default angular.module('mustangApp.modal', [])
  .factory('Modal', Modal)
  .name;
