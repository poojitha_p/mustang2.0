'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('requirements', {
    url: '/requirements',
    template: '<requirements></requirements>',
    authenticate: true
  });
}
