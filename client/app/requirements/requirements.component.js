import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './requirements.routes';

export class RequirementsController {
  $http;
  socket;
  requirements;
  newModule ={
    'name': '',
    'moduleId': '',
    'summary': 'some random summary'
  };
  // awesomeThings = [];
  // newThing = '';

  /*@ngInject*/
  constructor($http, $scope, socket) {
    this.$http = $http;
    this.socket = socket;

    // $scope.$on('$destroy', function() {
    //   socket.unsyncUpdates('thing');
    // });
  }

  $onInit() {
    this.$http.get('/api/requirements')
      .then(response => {
        this.requirements = response.data;
        // this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if(this.newThing) {
      this.$http.post('/api/things', this.newModule);
      this.newThing = '';
    }
  }

  addModule() {
    //if(this.newModule.name) {
      this.$http.post('/api/requirements/modules', this.newModule)
      .then(response =>{
          console.log(response.data);
      })
      .catch(err => {
        console.log(err);
      });
    //}
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }
}

export default angular.module('mustangApp.requirements', [uiRouter])
  .config(routing)
  .component('requirements', {
    template: require('./requirements.html'),
    controller: RequirementsController
  })
  .name;
