'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import uiSelect from 'ui-select';
// import arrayfilter from 'array-filter';
// import angularfilter from 'angular-filter';
import ngSanitize from 'angular-sanitize';

import 'angular-socket-io';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import datetimepicker from 'angular-ui-bootstrap-datetimepicker';
// import moment from 'moment';
import 'angular-validation-match';

import {
  routeConfig
} from './app.config';
import ngQuill from 'ng-quill';
import _Auth from '../components/auth/auth.module';
import Modal from '../components/modal/modal.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import requirements from './requirements/requirements.component';
import dashboard from './dashboard/dashboard.component';
import pawns from './pawns/pawns.module';
import employees from './employees/employees.module';
import PAWNS from './pawns/pawns.module';
import EMPLOYEES from './employees/employees.module';
import jobs from './jobs/jobs.module';
import timesheets from './timesheets/timesheets.module';
import clients from './clients/clients.module';
import templates from './templates/templates.module';
import departments from './departments/departments.module';
import payroll from './payroll/payroll.module';
import ticket from './ticket/ticket.module';
import leaverequest from './leaverequest/leaverequest.module.js';
import Holiday from './holiday/holiday.module';
import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';

import './app.css';

angular.module('mustangApp', [ngCookies, ngResource, ngSanitize, uiSelect, 'btford.socket-io', uiRouter,
  uiBootstrap, _Auth, Modal, account, admin, 'validation.match', navbar, footer, main, requirements, dashboard, constants,
  socket, pawns, employees, jobs, Holiday, departments, clients, templates, payroll, timesheets, leaverequest, ticket, 'ui.bootstrap.datetimepicker', util
])
  .config(routeConfig)
  .run(function($rootScope, $location, Auth) {
    'ngInject';
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function(event, next) {
      $rootScope.spinner ='true';
      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });

    $rootScope.$on('$stateChangeSuccess', function(event, next) {
      $rootScope.spinner ='false';
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['mustangApp'], {
      strictDi:true
    });
  });
