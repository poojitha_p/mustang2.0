import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  deleteClient(client) {
    var deleteConfirmationModal = this.modal.confirm.delete((client) => {
    this.$http.delete('/api/clients/'+client._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, client.name);
      this.$state.go('clients.list');
    }).catch(err => {
      console.log('client not deleted');
    });
  })
  deleteConfirmationModal(client.name, client);
  }

}
