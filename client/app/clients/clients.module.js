'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './clients.routes';

export default angular.module('mustangApp.clients', [uiRouter])
  .config(routing)
  .name;
