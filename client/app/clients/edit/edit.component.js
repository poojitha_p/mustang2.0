import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  updateClient(clientForm, client){
    if(clientForm.$invalid){
      this.submitted=true;
      return;
    }
    if( client.name){
    console.log("hi"+client);
    this.$http.put('/api/clients/'+client._id,{
      name:client.name,
      description:client.description,
      weburl:client.weburl,
      companysize:client.companysize,
      industry:client.industry,
      year:client.year,
      companytype:client.companytype,
      location:client.location
    }).then(res =>{
      this.$state.go('clients.info',{clientId:res.data._id},{reload:true});
    })
  }
}
  deleteClient(client) {
    console.log('deleting...');
    console.log(client);
    this.$http.delete(`/api/clients/${client._id}`);
    this.$state.go('clients.list');
  }

}
