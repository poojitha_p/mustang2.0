'use strict';

import {ClientComponent} from './clients/clients.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('clients', {
    url: '/clients',
    name: 'clients',
    abstract: true,
    authenticate: 'user',
    template: require('./clients/clients.html'),
    // component: 'clients',
    controllerAs: '$ctrl',
    controller: ClientComponent,
  });

  $stateProvider.state('clients.list', {
    url: '/list',
    name: 'clients.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'clients.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('clients.new', {
    url: '/new',
    name: 'clients.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'employees.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('clients.info', {
    url: '/:clientId',
    name: 'clients.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      clientId: function($stateParams){
        return $stateParams.clientId;
      },
      client: function($http, $stateParams){
        return $http.get('/api/clients/'+$stateParams.clientId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

  $stateProvider.state('clients.edit', {
    url: '/:clientId/edit',
    name: 'clients.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      clientId: function($stateParams){
        return $stateParams.clientId;
      },
      client: function($http, $stateParams){
        return $http.get('/api/clients/'+$stateParams.clientId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
