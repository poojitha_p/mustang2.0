import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newclient = {
    name: 'client',
    id:'',
    description: 'tell us something about your company.',
    weburl:'',
    companysize:'',
    industry: '',
    year:'',
    companytype:'',
    location: ''


  };

  industries  = ['Accounting','Architecture & planning','Broadcast Media','Business Supplies and Equipment',
                'Chemicals','Computer and Network Security','Financial Services','Government Relations','Information Services',
                'Information Technology and Services','Marketing and Advertising','Managemnet Consulting','Nanotechnology','Product','Research','Telecommunications','Utilities',
              'Wholesale'];
  companysizes = ['10-25 employees','25-50 employees','51-200 employees','201-500 employees','501-1,000 employees','5,001-10,000 employees','10,001+ employees'];
  companytypes = ['Global Company','Public Company','educational Institution','Self-Employed','Government Agency','Nonprofit','Sole Proprietorship','Private Held','Partnership'];
  message = 'Add a new client from here.';

  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {

  }

abbreviation(clientName){
  var res = clientName.split(' ');
  console.log(res);
  var abbreviation= '';
  for (var i = 0; i < res.length; i++) {
     var firstChar = res[i].charAt(0);
     abbreviation = abbreviation + firstChar;
  }
  console.log(abbreviation);
  this.newclient.id=abbreviation;
}
  addclient(clientForm) {
    if(clientForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newclient.name) {
      this.$http.post('/api/clients', this.newclient)
      .then( response => {
        //this.$state.go('pawns.info', {name: response.data.name});
        this.$state.go('clients.info', {clientId: response.data._id});
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }

  resetForm(){
    this.newclient = {
      name: 'client',
      id:'',
      description: '',
      weburl:'',
      companysize:'',
      industry:'',
      year:'',
      companytype:'',
      location:'',

    };
  }

}
