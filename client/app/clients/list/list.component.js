import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  ListComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('/api/clients')
      .then(response => {
        this.clients = response.data;
      }).catch(err => {

      })
  }

  deleteClient(client) {
    this.$http.delete(`/api/clients/${client._id}`);
  }
}
