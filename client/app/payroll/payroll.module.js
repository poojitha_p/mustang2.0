'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './payroll.routes';

export default angular.module('mustangApp.payroll', [uiRouter])
  .config(routing)
  .name;
