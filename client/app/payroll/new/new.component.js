import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newPayroll = {
    Ctc: '',
    Variable: '',
    Hra: '',
    Pf: '',
    Basicallowance: '',
    Pt: '',
    Travelallowance: '',
    Emi: '',
    Medicalallowance: '',
    Esi: '',
    Subtotal: '',
    Total:'',
    Grosstotal: '',
  };



  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  addPayroll() {
    if(this.newPayroll.Ctc) {
      this.$http.post('/api/payroll', this.newPayroll)
      .then(response => {
        //this.$state.go('payroll.info', {name: response.data.name});
        this.$state.go('payroll.info', {payrollId:response.data._id});
        // this.resetForm();
      }).catch( err => {
        console.log(err);
        console.log('oops');
      });

    }
  }

  resetForm(){
    this.newPayroll = {
      Ctc: '',
      Variable: '',
      Hra: '',
      Pf: '',
      Basicallowance: '',
      Pt: '',
      Travelallowance: '',
      Emi: '',
      Medicalallowance: '',
      Esi: '',
      Subtotal: '',
      Total: '',
      Grosstotal: '',
    };
  }

  //  SalaryCalculation(gross){
  //    if(gross>=1000000){
  //      this.newPayroll.Hra =  gross*40/100;
  //    }else{
  //      this.newPayroll.Hra =  gross*40/100;
  // Variable+Hra+Basicallowance+Travelallowance+Medicalallowance
  //    }
  // }
  SalaryCalculation(Ctc){
    this.newPayroll.Variable = Ctc*20/100;
    this.newPayroll.Hra = Ctc*40/100;
    this.newPayroll.Basicallowance = Ctc*10/100;
    this.newPayroll.Travelallowance = Ctc*30/100;
    this.newPayroll.Medicalallowance = Ctc*30/100;
    this.newPayroll.Subtotal = this.newPayroll.Variable+this.newPayroll.Hra+this.newPayroll.Basicallowance
    +this.newPayroll.Travelallowance+this.newPayroll.Medicalallowance;
    this.newPayroll.Pf = Ctc*12/100;
    this.newPayroll.Pt = Ctc*12/100;
    this.newPayroll.Emi = Ctc*15/100;
    this.newPayroll.Esi = Ctc*1.75/100;
    this.newPayroll.Total = this.newPayroll.Pf+this.newPayroll.Pt+this.newPayroll.Emi+this.newPayroll.Esi;
    this.newPayroll.Grosstotal = this.newPayroll.Subtotal-this.newPayroll.Total;
    console.log(this.newPayroll.Grosstotal);
  }


// SalaryCalculation(Ctc){
// this.newPayroll.Hra = Ctc*40/100;
// this.newPayroll.Travelallowance = Ctc*10/100;
// this.newPayroll.Medicalallowance = Ctc*30/100;
// this.newPayroll.Subtotal = this.newPayroll.Hra+this.newPayroll.Travelallowance+this.newPayroll.Medicalallowance;
// this.newPayroll.Grosstotal = this.newPayroll.Subtotal;
// this.newPayroll.Pf = Ctc*12/100;
// this.newPayroll.Pt = Ctc*12/100;
// this.newPayroll.Esi = Ctc*1.75/100;
// if(Ctc<=250000){
// this.newPayroll.It = Ctc*0/100;
// }else{
// if(Ctc>=250000){
// this.newPayroll.It = Ctc*5/100;


}
