'use strict';

import {PayrollComponent} from './payroll/payroll.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('payroll', {
    url: '/payroll',
    name: 'payroll',
    abstract: true,
    authenticate: 'user',
    template: require('./payroll/payroll.html'),
    // component: 'payroll',
    controllerAs: '$ctrl',
    controller: PayrollComponent,
  });

  $stateProvider.state('payroll.list', {
    url: '/list',
    name: 'payroll.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'payroll.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('payroll.new', {
    url: '/new',
    name: 'payroll.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'payroll.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('payroll.info', {
    url: '/:payrollId',
    name: 'payroll.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      payrollId: function($stateParams){
        return $stateParams.payrollId;
      },
      payroll: function($http, $stateParams){
        return $http.get('/api/payroll/'+$stateParams.payrollId)
          .then(response => {
            return response.data;
          });
      }
    },
    // component: 'payroll.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

$stateProvider.state('payroll.edit', {
    url: '/:payrollId/edit',
    name: 'payroll.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      payrollId: function($stateParams){
        return $stateParams.payrollId;
      },
      payroll: function($http, $stateParams){
        return $http.get('/api/payroll/'+$stateParams.payrollId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'payroll.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
