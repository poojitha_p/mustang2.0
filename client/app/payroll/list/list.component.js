import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  ListComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;


  /*@ngInject*/
  constructor($http, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('/api/payroll')
      .then(response => {
        this.payroll = response.data;
      }).catch(err => {
  })
  }
   deletepayroll(payroll) {
    this.$http.delete(`/api/payroll/${payroll._id}`);
  }
}
