import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  editComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;



  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedIn;
    Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  updatepayroll(payroll){
    console.log("hi"+payroll);
      this.$http.put('/api/payroll/'+payroll._id,{
        Ctc: payroll.Ctc,
        Variable: payroll.Variable,
        Hra: payroll.Hra,
        Pf: payroll.Pf,
        Basicallowance: payroll.Basicallowance,
        Pt: payroll.Pt,
        Travelallowance: payroll.Travelallowance,
        Emi: payroll.Emi,
        Medicalallowance: payroll.Medicalallowance,
        Esi:payroll.Esi,
        Subtotal: payroll.Subtotal,
        Total: payroll.Total,
        Grosstotal: payroll.Grosstotal

      }).then(res =>{
        this.$state.go('payroll.info',{payrollId:res.data._id},{reload:true});
       })
     }
  deletepayroll(payroll) {
    console.log('deleting...');
    console.log(payroll);
    this.$http.delete(`/api/payroll/${payroll._id}`);
    this.$state.go('payroll.list');
  }

}
