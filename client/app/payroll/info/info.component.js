import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  InfoComponent {
  $http;
  $state;
  $stateParams;
  Modal;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;



  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth,Modal) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.modal= Modal;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedIn;
    Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
   }

   deletePayroll(payroll) {
     var deleteConfirmationModal = this.modal.confirm.delete((payroll) => {
     this.$http.delete('/api/payroll/'+payroll._id,{
     }).then(response=>{
       console.log(payroll._id);
       var modalAlert = this.modal.confirm.modalAlert(response, payroll._id);
       this.$state.go('payroll.list');
     }).catch(err => {
       console.log('payroll not deleted');
     });
   })
   deleteConfirmationModal(payroll.payrollId, payroll);
   }

   }
