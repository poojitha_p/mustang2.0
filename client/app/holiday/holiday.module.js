'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './holiday.routes';

export default angular.module('mustangApp.holiday', [uiRouter])
  .config(routing)
  .name;
