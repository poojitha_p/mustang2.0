import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  ListComponent {
  $http;
  $state;
  socket;
  $filter;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  holidaylist=[];
  fromDate;
  toDate;

  /*@ngInject*/
  constructor($http, $scope, socket, $state, $filter, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$filter = $filter;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('api/holiday/')
      .then(response => {
        this.holidaylist= response.data;
      });
  }

    deleteholiday(holiday) {
    this.$http.delete(`/api/holiday/${holiday._id}`)
    .then(response => {
      // this.holidaylist = filteredlist;
    }).catch(err =>{
    console.err("could not delete holidays" +err);
    })

    }

    fetchHolidays(fromDate,toDate) {
    this.$http.get('api/holiday/')
    .then(response => {
      var filtered = [];
      var from_date = Date.parse(fromDate);
      var to_date = Date.parse(toDate);
      angular.forEach(response.data, function(item) {
        var date = Date.parse(item.date);
           if(date > from_date && date < to_date) {
              filtered.push(item);
            }
          });
          this.holidaylist = filtered;
        })
      }

      reset() {
        this.$http.get('api/holiday/')
        .then(response => {
          this.fromDate = null;
          this.toDate = null;
          this.holidaylist= response.data;
        });
    }



}
