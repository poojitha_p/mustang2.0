import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  $filter;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newholiday = {
    title: '',
    date: '',
  summary: '',
  };

  message = 'Add a new holiday from here.';

   holidaylist = [];



  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth, $filter, Modal) {
    // 'ngInject';
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.$filter = $filter;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {

  }

  addHoliday(holidays) {
    if(this.newholiday.title) {
      this.$http.post('/api/holiday', this.newholiday)
      .then( response => {
        this.$state.go('holiday.info', {holidayId: response.data._id});
      }).catch( err => {
        console.log('oops');
      });
    }
  }


  resetForm(){
    this.holiday = {
      tille: 'holiday',
      date: 'date',
    summary: '',

    };
  }

}
