import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  $filter;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  holiday = {};

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, $filter, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.$filter = $filter;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }


  updateholiday(holiday){
      this.$http.put('api/holiday/'+holiday._id,{
         date: holiday.date,
         title: holiday.title,
        summary: holiday.summary,
        }).then(res =>{

          this.$state.go('holiday.info',{holidayId:holiday._id},{reload:true});
        })
    }

    convertDate(date){
       this.holiday.date = new Date(date);
    }


  deleteholiday(holiday) {
    console.log('deleting...');
    console.log(holiday);
    this.$http.delete(`api/holiday/${holiday._Id}`);
    this.$state.go('holiday.list');
  }

}
