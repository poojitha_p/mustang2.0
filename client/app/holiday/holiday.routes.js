'use strict';

import {HolidayComponent} from './hoilday/holiday.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('holiday', {
    url: '/holiday',
    name: 'holiday',
    abstract: true,
    authenticate: 'user',
    template: require('./hoilday/holiday.html'),
    // component: 'employees',
    controllerAs: '$ctrl',
    controller: HolidayComponent,
  });

  $stateProvider.state('holiday.list', {
    url: '/list',
    name: 'holiday.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'employees.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('holiday.new', {
    url: '/new',
    name: 'holiday.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'employees.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('holiday.info', {
    url: '/:holidayId',
    name: 'holiday.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      holidayId: function($stateParams){
        return $stateParams.holidayId;
      },
      holiday: function($http, $stateParams){
        return $http.get('/api/holiday/'+$stateParams.holidayId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

  $stateProvider.state('holiday.edit', {
    url: '/:holidayId/update',
    name: 'holiday.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      holidayId: function($stateParams){
        return $stateParams.holidayId;
      },
      holiday: function($http, $stateParams){
        return $http.get('/api/holiday/'+$stateParams.holidayId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
