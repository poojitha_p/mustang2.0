import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  holiday;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
    this.holiday = {};
  }

  $onInit() {
  }

  convertDate(date){
     this.holiday.date = new Date(date);
  }

  deleteHoliday(holiday) {
    console.log('deleting...');
    console.log(holiday);
    this.$http.delete('api/holiday/'+holiday._id);
    this.$state.go('holiday.list');
  }

}
