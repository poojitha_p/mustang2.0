import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './dashboard.routes';

export class    DashboardController {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  todos=[];
  todo = {
    title: 'do something',
    summary: 'describe your something',
    id: '',
  };

  /*@ngInject*/
  constructor($http, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('/api/todos')
      .then(response => {
        this.todos = response.data;
      });
  }

  addTodo() {
    if(this.todo.title) {
      this.$http.post('/api/todos', this.todo)
      .then( response => {
        this.$state.reload();
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }

  reset(){
    this.todo = {
      title: 'do something',
      summary: 'describe your something',
      id: '',
    };
  }

  deleteTodo(todo) {
    this.$http.delete(`/api/todos/${todo._id}`);
  }
}

export default angular.module('mustangApp.dashboard', [uiRouter])
  .config(routing)
  .component('dashboard', {
    template: require('./dashboard.html'),
    controller: DashboardController
  })
  .name;
