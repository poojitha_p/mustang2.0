import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;



  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  getConvertDate(date){
    return new Date(date);
  }

  updateEmployee(employeeForm, employee) {
    if(employeeForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newemployee.empId) {
      this.$http.put('/api/employees/'+employee._id,{
      empId:this.newemployee.empId,
      fathername:this.newemployee.fathername,
      pan:this.newemployee.panId,
      nationality:this.newemployee.nationality,
      aadhar:this.newemployee.aadhar,
      designation:this.newemployee.designation,
      department:this.newemployee.department._id,
      martial:this.newemployee.martial,
      spouse:this.newemployee.SpouseName,
      currentadrress:this.newemployee.currentadrress,
      currentadrressperiodofstay:this.newemployee.POS,
      permanentaddress:this.newemployee.permanentaddress,
      permanentaddressperiodofstay:this.newemployee.pos,
      education:{
        highschool: {
          schoolname: this.newemployee.Insname,
          dateofcompletion:this.newemployee.tenthpassedout,
          score: this.newemployee.tenthpercentage,
        },
        intermediate: {
          schoolname: this.newemployee.insname,
          specialization:this.newemployee.specialization,
          dateofcompletion: this.newemployee.Interpassedout,
          score: this.newemployee.Intermediate,
        },
        graduation: {
          collegename: this.newemployee.Uniname,
          specialization:this.newemployee.gradspeci,
          from:this.newemployee.From,
          to:this.newemployee.To,
          score: this.newemployee.graduationpassedout
        },
        postgraduation:{
          collegename:  this.newemployee.uniname,
          specialization:this.newemployee.postspeci,
          from: this.newemployee.FROM,
          to: this.newemployee.TO,
          score: this.newemployee.postgraduationpercentage,
        },
        doctorate: {
          specialization:this.newemployee.pHDSpec,
          university: this.newemployee.universityname,
          from: this.newemployee.pHDfrom,
          to: this.newemployee.pHDto,
          score: this.newemployee.pHDpercentage,
        }
      },
      employment: [
        {
          companyname: this.newemployee.companyname,
          from: this.newemployee.From,
          to: this.newemployee.To,
          designation: this.newemployee.designations,
        }
      ]
      })
      .then( response => {
        this.$state.go('employees.info', {employeeId: response.data._id});
      }).catch( err => {
        console.log('oops');
      });
    }
  }


  deleteEmployee(employee) {
    console.log('deleting...');
    console.log(employee);
    this.$http.delete(`/api/employees/${employee._id}`);
    this.$state.go('employee.list');
  }

}
