'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './employees.routes';

export default angular.module('mustangApp.employees', [uiRouter])
  .config(routing)
  .name;
