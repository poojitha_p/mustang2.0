'use strict';

import {EmployeesComponent} from './employees/employees.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('employees', {
    url: '/employees',
    name: 'employees',
    abstract: true,
    authenticate: 'user',
    template: require('./employees/employees.html'),
    // component: 'employees',
    controllerAs: '$ctrl',
    controller: EmployeesComponent,
  });

  $stateProvider.state('employees.list', {
    url: '/list',
    name: 'employees.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'employees.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('employees.new', {
    url: '/new',
    name: 'employees.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'employees.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

    $stateProvider.state('employees.basic', {
      url: '/:employeeId/basic',
      name: 'employees.basic',
      authenticate: 'user',
      resolve: {
        employeeId: function($stateParams){
          return $stateParams.employeeId;
        },
        employee: function($http, $stateParams){
          return $http.get('/api/employees/'+$stateParams.employeeId)
            .then(response => {
              return response.data;
            });
        }
      },
      template: require('./new/new.basic.html'),
      // component: 'employees.list',
      controllerAs: '$ctrl',
      controller: NewComponent,
    });

  $stateProvider.state('employees.info', {
    url: '/:employeeId',
    name: 'employees.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      employeeId: function($stateParams){
        return $stateParams.employeeId;
      },
      employee: function($http, $stateParams){
        return $http.get('/api/employees/'+$stateParams.employeeId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

  $stateProvider.state('employees.edit', {
    url: '/:employeeId/update',
    name: 'employees.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      employeeId: function($stateParams){
        return $stateParams.employeeId;
      },
      employee: function($http, $stateParams){
        return $http.get('/api/employees/'+$stateParams.employeeId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });

}
