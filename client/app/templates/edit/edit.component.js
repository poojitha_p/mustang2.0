import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }


  updatetemplate(templateForm, template){
    if(templateForm.$invalid){
      this.submitted=true;
      return;
    }
    if( template.templatename){
    console.log("hi"+template);
    this.$http.put('/api/templates/'+template._id,{
      templatename:template.templatename,
      description:template.description
    }).then(res =>{
      this.$state.go('templates.info',{templateId:res.data._id},{reload:true});
    })
  }
}
  deletetemplate(template) {
    console.log('deleting...');
    console.log(template);
    this.$http.delete(`/api/templates/${template._id}`);
    this.$state.go('templates.list');
  }

}
