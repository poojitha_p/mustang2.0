import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  convertDate(date){
     this.template.date = new Date(date);
  }
  deletetemplate(template) {
    console.log(template);
    var deleteConfirmationModal = this.modal.confirm.delete((template) => {
    this.$http.delete('/api/templates/'+template._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, template.templatename);
      this.$state.go('templates.list');
    }).catch(err => {
      console.log('template not deleted');
    });
  })
  deleteConfirmationModal(template.templatename, template);
  }

}
