'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './templates.routes';

export default angular.module('mustangApp.templates', [uiRouter])
  .config(routing)
  .name;
