'use strict';

import {TemplateComponent} from './templates/templates.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('templates', {
    url: '/templates',
    name: 'templates',
    abstract: true,
    authenticate: 'user',
    template: require('./templates/templates.html'),
    // component: 'templates',
    controllerAs: '$ctrl',
    controller: TemplateComponent,
  });

  $stateProvider.state('templates.list', {
    url: '/list',
    name: 'templates.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'templates.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('templates.new', {
    url: '/new',
    name: 'templates.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'employees.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('templates.info', {
    url: '/:templateId/info',
    name: 'templates.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      templateId: function($stateParams){
        return $stateParams.templateId;
      },
      template: function($http, $stateParams){
        return $http.get('/api/templates/'+$stateParams.templateId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
  $stateProvider.state('templates.generate', {
    url: '/:templateId/generate',
    name: 'templates.generate',
    authenticate: 'user',
    template: require('./info/generate.html'),
    resolve: {
      templateId: function($stateParams){
        return $stateParams.templateId;
      },
      template: function($http, $stateParams){
        return $http.get('/api/templates/'+$stateParams.templateId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

  $stateProvider.state('templates.edit', {
    url: '/:templateId/edit',
    name: 'templates.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      templateId: function($stateParams){
        return $stateParams.templateId;
      },
      template: function($http, $stateParams){
        return $http.get('/api/templates/'+$stateParams.templateId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
