import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';
import handlebars from 'handlebars/dist/handlebars.js';
// import fs from 'fs';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  // newvalues=[];
  newtemplate = {
    name:'',
    description:'Dear <b>{{name}}</b>,'+
                '<br><br><b>{{companyname}}</b> is excited to bring you on board as <b>{{jobtitle}}</b>.'+
                '<br><br>We’re just a few formalities away from getting down to work. Please take the time to'+
                '<br>review our offer. It includes important details about your compensation, benefits and'+
                '<br>the terms and conditions of your anticipated employment with <b>{{companyname}}</b>.'+
                '<br><br><b>{{companyname}}</b> is offering a <b>{{type}}</b> position for you as <b>{{jobtitle}}</b>,'+
                '<br>reporting to manager starting on proposed start date at'+
                '<br><b>{{location}}</b>. Expected hours of work are days of week and hours of work.'+
                '<br>In this position, <b>{{companyname}}</b> is offering to start you at a pay rate of per year.'+
                '<br><br>You will be paid on a monthly basis, starting date of next pay period.'+
                '<br>As part of your compensation, were also offering If applicable, bonus. '+
                '<br><br>Please indicate your agreement with these terms and accept this offer by signing and dating'+
                '<br>this agreement on or before offer expiration date.'+
                '<br><br>Signature:'+
                '<br><br>Name:<b>{{name}}</b>'+
                '<br>Date:<b>{{date}}</b>',
    templatename:'',
    tid:'',
    value:''

  };

  params= [
    {
    name: 'name',
    value: 'Sainath'
    },
    {
    name: 'companyname',
    value: 'redpawns'
    },
    {
    name: 'date',
    value: '10-12-1996'
    },
    {
      name:'type',
      value:'fulltime'
    },
    {
      name:'jobtitle',
      value:'Developer'
    },
    {
      name:'location',
      value:'bangalore'
    }

  ];
  param ={
    name: '',
    value: ''
  }


  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth, $document) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.$document = $document;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {

  }
 add(){
   this.add = true;
 }

  updateParam(){
    var template = handlebars.compile(this.newtemplate.description);
    // var heading = handlebars.compile(this.newtemplate.heading);
    this.result = template(this.convert(this.params));
    console.log(this.result);
  }

  updateTemplate(params){
    var template = handlebars.compile(this.newtemplate.description);
    // var heading = handlebars.compile(this.newtemplate.heading);
    this.result = template(this.convert(params));
    console.log(this.result);
  }

  lookupParams(str){
    //const str = this.newtemplate.description;
    var regex = /{{(.*?)}}/ig;
    var res = this.getAllMatches(regex, str);
    // console.log(res.length);
    var items =[];
    res.forEach(function (item) {
        // console.log(item);
        items.push(item);
        console.log(items);
      // if(res.length){
      //   for(var i=0; i<res.length; i++){
      //     for(var j=0;j<this.params.length;j++){
      //       console.log(res[i].includes(this.params[j].name));
      //       // console.log(this.params[j].name);
      //       if(res[i].includes(this.params[j].name)==false){
      //         console.log(res[i]);
      //         // console.log(this.params[j]);
      //       }
      //     }
      //   }
      // }
    });


// for (var i=0; i< params.name;i++)
//   for (var j=0;j< item.name ;j++)
//   {
//     if (params[i]!== item.name[i]){
//       console.log(this.item)
//     }0
//   }
// }

//
// unmatched = item.filter(function (a) {
//     return params.indexOf(a) === -1;
// });
// console.log(unmatched);

    // const regex = /{(.*?)}}/ig;
    // //const str = `this is my {{text}}`;
    // let m;
    //
    // if ((m = regex.exec(str)) !== null) {
    //     // The result can be accessed through the `m`-variable.
    //     m.forEach((match, groupIndex) => {
    //         console.log(`Found match, group ${groupIndex}: ${match}`);
    //     });
    // }
    //const str = `this is my {{text}}`;
    // let m;
    //
    // if ((m = regex.exec(str)) !== null) {
    //     // The result can be accessed through the `m`-variable.
    //     m.forEach((match, groupIndex) => {
    //         console.log(`Found match, group ${groupIndex}: ${match}`);
    //     });
    // };
  }




  getAllMatches(regex, text) {
      if (regex.constructor !== RegExp) {
          throw new Error('not RegExp');
      }

      var res = [];
      var match = null;

      if (regex.global) {
          while (match = regex.exec(text)) {
              res.push(match);
          }
      }
      else {
          if (match = regex.exec(text)) {
              res.push(match);
          }
      }

      return res;
  }



  convert(params){
    var oparams ={};
    for (var i = 0; i < params.length; i++) {
      oparams[params[i].name] = params[i].value;
    }
    return oparams;
  }

  addParam (param){
    console.log('new param to be added');
    // console.log(param)    ;
    this.params.push(param);
    console.log(this.params.length);
  }
  // addParam(param) {
  //   if(this.params.name) {
  //     this.$http.post('/api/templates', this.params)
  //     .then( response => {
  //       this.$state.reload();
  //       this.resetForm();
  //     }).catch( err => {
  //       console.log('oops');
  //     });
  //   }
  // }

  addtemplate(templateForm) {
    console.log('hi');
    if(templateForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newtemplate.templatename) {
      this.$http.post('/api/templates',{
        templatename:this.newtemplate.templatename,
        description:this.result,
        tid:this.newtemplate.tid,
        params: this.params
      })
      .then( response => {
        this.$state.go('templates.info', {templateId: response.data._id});
      }).catch( err => {
        console.log('oops');
      });

    }
  }


  resetForm(){
    this.newtemplate = {
      name:'',
      value:'',
      description: '',
      templatename:'',
      id:'',
    };
  }

}
