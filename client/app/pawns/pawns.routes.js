'use strict';

import {PawnsComponent} from './pawns/pawns.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('pawns', {
    url: '/pawns',
    name: 'pawns',
    abstract: true,
    authenticate: 'user',
    template: require('./pawns/pawns.html'),
    // component: 'pawns',
    controllerAs: '$ctrl',
    controller: PawnsComponent,
  });

  $stateProvider.state('pawns.list', {
    url: '',
    name: 'pawns.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'pawns.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('pawns.new', {
    url: '/new',
    name: 'pawns.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'pawns.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('pawns.info', {
    url: '/:pawnId',
    name: 'pawns.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {

      pawnId: function($stateParams){
        return $stateParams.pawnId;
      },
      pawn: function($http, $stateParams){
        return $http.get('/api/pawns/'+$stateParams.pawnId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'pawns.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
}
