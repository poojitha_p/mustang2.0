'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './pawns.routes';

export default angular.module('mustangApp.pawns', [uiRouter])
  .config(routing)
  .name;
