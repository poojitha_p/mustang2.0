import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newpawn = {
    name: 'pawn',
    color: 'white',
    summary: 'tell us something about your pawn.'
  };

  message = 'Add a new pawn from here.';


  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  addPawn() {
    if(this.newpawn.name) {
      this.$http.post('/api/pawns', this.newpawn)
      .then( response => {

        //this.$state.go('pawns.info', {name: response.data.name});
        this.$state.go('pawns.info', {pawnId: response.data._id});
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }

  resetForm(){
    this.newpawn = {
      name: 'pawn',
      color: 'white',
      summary: '',
    };
  }

}
