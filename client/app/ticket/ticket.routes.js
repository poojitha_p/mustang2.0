'use strict';

import {TicketComponent} from './ticket/ticket.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('ticket', {
    url: '/ticket',
    name: 'ticket',
    abstract: true,
    authenticate: 'user',
    template: require('./ticket/ticket.html'),
    // component: 'pawns',
    controllerAs: '$ctrl',
    controller: TicketComponent,
  });

  $stateProvider.state('ticket.list', {
    url: '/list',
    name: 'ticket.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'pawns.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('ticket.new', {
    url: '/new',
    name: 'ticket.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'pawns.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('ticket.info', {
    url: '/:ticketId',
    name: 'ticket.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {

      ticketId: function($stateParams){
        return $stateParams.ticketId;
      },
      ticket: function($http, $stateParams){
        return $http.get('/api/tickets/'+$stateParams.ticketId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'pawns.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
  $stateProvider.state('ticket.edit', {
    url: '/:ticketId/edit',
    name: 'ticket.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      ticketId: function($stateParams){
        return $stateParams.ticketId;
      },
      ticket: function($http, $stateParams){
        return $http.get('/api/tickets/'+$stateParams.ticketId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
