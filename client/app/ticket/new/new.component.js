import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newticket = {
    subject:'Untitiled',
    description:'',
    status:'',
    type:'',
    owner:'',
    assignee:'',
    comments:'',
  };

  status  = ['New','Assigned','Open','Resolved','On hold','Closed'];
  type = ['Feature request', 'Change request', 'Account/Subscription'];
  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInjectcomments';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  addTicket(ticketForm) {
    if(ticketForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.newticket.subject) {
      this.$http.post('/api/tickets',{
        subject:this.newticket.subject,
      description:this.newticket.description,
      // status:this.newticket.status,
      type:this.newticket.type,
      // owner:this.newticket.owner,
      // assignee:this.newticket.assignee,
      // comments:this.newticket.comments,
      })
      .then( response => {
        //this.$state.go('pawns.info', {name: response.data.name});
        this.$state.go('ticket.info');
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }
  // addticket() {
  //   console.log(this.newticket)
  //   if(this.newticket.title) {
  //     this.$http.post('/api/tickets', this.newticket)
  //     .then( response => {
  //       //this.$state.go('pawns.info', {name: response.data.name});
  //       this.$state.go('tickets.info', {ticketId: response.data._id});
  //       this.resetForm();
  //     }).catch( err => {
  //       console.log('oops');
  //     });
  //
  //   }
  // }

  resetForm(){
    this.newticket = {
      subject:'',
      description:'',
      status:'',
      type:'',
      owner:'',
      assignee:'',
      comments:'',

    };
  }

}
