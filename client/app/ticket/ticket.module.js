'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './ticket.routes';

export default angular.module('mustangApp.ticket', [uiRouter])
  .config(routing)
  .name;
