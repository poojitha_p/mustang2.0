import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;


Assigne = ['Avinash', 'Sainath', 'Poojitha', 'Sunil', 'Pavan raju', 'Kulasekhar', 'Manjunatha', 'Srinu', 'Salman', 'Bharath chetty'];
  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }
  $onInit() {
  }
  getConvertDate(date){
    return new Date(date);
  }

  updateTicket(myForm, ticket) {
    if(myForm.$invalid){
      this.submitted=true;
      return;
    }
    if(this.editticket.ticketId) {
      this.$http.put('/api/tickets/'+ticket._id,{
      ticketId:this.editticket.ticketId,
    })
    .then(response => {
      this.$state.go('ticket.info', {ticketId: response.data._id});
    }).catch( err => {
      console.log('oops');
    });
  }
}
}
