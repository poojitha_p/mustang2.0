import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.modal = Modal;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('/api/tickets')
      .then(response => {
        this.ticket = response.data;
      }).catch(err => {
  })
  }
  deleteTicket(ticket) {
    var deleteConfirmationModal = this.modal.confirm.delete((ticket) => {
    this.$http.delete('/api/tickets/'+ticket._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, ticket.subject);
      this.$state.go('ticket.list');
    }).catch(err => {
      console.log('ticket not deleted');
    });
  })
  deleteConfirmationModal(ticket.subject, ticket);
  }
}
// deleteClient(client) {
//   var deleteConfirmationModal = this.modal.confirm.delete((client) => {
//   this.$http.delete('/api/clients/'+client._id,{
//   }).then(response=>{
//     var modalAlert = this.modal.confirm.modalAlert(response, client.name);
//     this.$state.go('clients.list');
//   }).catch(err => {
//     console.log('client not deleted');
//   });
// })
// deleteConfirmationModal(client.name, client);
// }

//   deleteticket(ticket) {
//     this.$http.delete(`/api/ticket/${ticket._id}`);
//   }
// }
