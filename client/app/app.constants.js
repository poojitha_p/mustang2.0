'use strict';

import angular from 'angular';

export default angular.module('mustangApp.constants', ['ngQuill'])
  .constant('appConfig', require('../../server/config/environment/shared'))
  .name;
