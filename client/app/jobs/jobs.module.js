'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './jobs.routes';

export default angular.module('mustangApp.jobs', [uiRouter])
  .config(routing)
  .name;
