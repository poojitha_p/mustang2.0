'use strict';

import {JobComponent} from './jobs/jobs.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('jobs', {
    url: '/jobs',
    name: 'jobs',
    abstract: true,
    authenticate: 'user',
    template: require('./jobs/jobs.html'),
    // component: 'pawns',
    controllerAs: '$ctrl',
    controller: JobComponent,
  });

  $stateProvider.state('jobs.list', {
    url: '/list',
    name: 'jobs.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'pawns.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('jobs.new', {
    url: '/new',
    name: 'jobs.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'pawns.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('jobs.info', {
    url: '/:jobId',
    name: 'jobs.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {

      jobId: function($stateParams){
        return $stateParams.jobId;
      },
      job: function($http, $stateParams){
        return $http.get('/api/jobs/'+$stateParams.jobId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'pawns.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
  $stateProvider.state('jobs.edit', {
    url: '/:jobId/edit',
    name: 'jobs.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      jobId: function($stateParams){
        return $stateParams.jobId;
      },
      job: function($http, $stateParams){
        return $http.get('/api/jobs/'+$stateParams.jobId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
