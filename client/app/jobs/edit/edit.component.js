import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  updateJob(jobForm,job){
    if(jobForm.$invalid){
      this.submitted=true;
      return;
    }
    if(job.title){
      console.log("hi"+job);
      this.$http.put('/api/jobs/'+job._id,{
          title:job.title,
          description:job.description,
          keywords:job.keywords,
          annual:job.annual,
          type:job.type,
          nov:job.nov,
          np:job.np,
          experience:job.experience,
          industry:job.industry,
          role:job.role,
          location:job.role,
          area:job.area
      }).then(res =>{
        this.$state.go('jobs.info',{jobId:res.data._id},{reload:true});
      })
  }
}

  deleteJob(job) {
    console.log('deleting...');
    console.log(job);
    this.$http.delete(`/api/jobs/${job._id}`);
    this.$state.go('jobs.list');
  }

}
