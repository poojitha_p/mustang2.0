import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    // 'ngInject';
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  // deleteJob(job) {
  //   console.log('deleting...');
  //   console.log(job);
  //   this.$http.delete(`/api/jobs/${job._id}`);
  //   this.$state.go('jobs.list');
  // }
  deleteJob(job) {
    var deleteConfirmationModal = this.modal.confirm.delete((job) => {
    this.$http.delete('/api/jobs/'+job._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, job.title);
      this.$state.go('jobs.list');
    }).catch(err => {
      console.log('job not deleted');
    });
  })
  deleteConfirmationModal(job.title, job);
  }

}
