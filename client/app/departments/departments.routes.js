'use strict';

import {DepartmentsComponent} from './departments/departments.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';


export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('departments', {
    url: '/departments',
    name: 'departments',
    abstract: true,
    authenticate: 'user',
    template: require('./departments/departments.html'),
    // component: 'departments',
    controllerAs: '$ctrl',
    controller: DepartmentsComponent,
  });

  $stateProvider.state('departments.list', {
    url: '/list',
    name: 'departments.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'departments.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('departments.new', {
    url: '/new',
    name: 'departments.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'departments.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('departments.info', {
    url: '/:departmentsId',
    name: 'departments.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {
      departmentsId: function($stateParams){
        return $stateParams.departmentsId;
      },
      departments: function($http, $stateParams){
        return $http.get('/api/departments/'+$stateParams.departmentsId)
          .then(response => {
            return response.data;
          });
      }
    },
    // component: 'departments.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });

$stateProvider.state('departments.edit', {
    url: '/:departmentsId/edit',
    name: 'departments.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      departmentsId: function($stateParams){
        return $stateParams.departmentsId;
      },
      departments: function($http, $stateParams){
        return $http.get('/api/departments/'+$stateParams.departmentsId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'departments.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
