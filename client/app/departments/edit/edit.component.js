import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  editComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;



  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedIn;
    Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  updatedepartments(departments){
    console.log("hi"+departments);
      this.$http.put('/api/departments/'+departments._id,{
         name:departments.name,
         id:departments.id,
       summary:departments.summary
      }).then(res =>{
        this.$state.go('departments.info',{departmentsId:res.data._id},{reload:true});
       })
     }
  deletedepartments(departments) {
    console.log('deleting...');
    console.log(departments);
    this.$http.delete(`/api/departments/${departments._id}`);
    this.$state.go('departments.list');
  }

}
