import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  InfoComponent {
  $http;
  $state;
  $stateParams;
  Modal;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;



  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth,Modal) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.modal=Modal;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedIn;
    Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }


  deleteDepartments(departments) {
    var deleteConfirmationModal = this.modal.confirm.delete((departments) => {
    this.$http.delete('/api/departments/'+departments._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, departments.name);
      this.$state.go('departments.list');
    }).catch(err => {
      console.log('departments not deleted');
    });
  })
  deleteConfirmationModal(departments.name, departments);
  }

}
