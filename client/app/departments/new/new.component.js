import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newdepartments = {
    name: '',
    id: '',
    summary: 'Tell us something about your department.'
  };

  departments = ['HR','FINANCE','SALES & MARKETING','ENGINEERING','PRODUCTION','RESEARCH & DEVELOPMENT'];


  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  addDepartments() {
    if(this.newdepartments.name) {
      this.$http.post('/api/Departments', this.newdepartments)
      .then(response => {
        //this.$state.go('departments.info', {name: response.data.name});
        this.$state.go('departments.info', {departmentsId:response.data._id});
        // this.resetForm();
      }).catch( err => {
        console.log(err);
        console.log('oops');
      });

    }
  }

  resetForm(){
    this.newdepartments = {
      name: 'departments',
      id: '',
      summary: '',
    };
  }

}
