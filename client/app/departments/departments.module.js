'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './departments.routes';

export default angular.module('mustangApp.departments', [uiRouter])
  .config(routing)
  .name;
