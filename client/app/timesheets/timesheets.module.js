'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './timesheets.routes';

export default angular.module('mustangApp.timesheets', [uiRouter])
  .config(routing)
  .name;
