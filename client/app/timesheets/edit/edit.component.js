import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }


  saveTimesheet(timesheet){
  // {
  //   if(jobForm.$invalid){
  //     this.submitted=true;
  //     return;
  //   }
  //   if(job.title){
      console.log("hi"+timesheet);
      this.$http.put('/api/timesheets/'+timesheet._id,{
        weeknumber:timesheet.weeknumber,
        year:timesheet.year,
        empId:timesheet.empId,
        from:timesheet.from,
        to:timesheet.to,
        tsdate:timesheet.tsdate,
        manhours:timesheet.manhours,
        total:timesheet.total,
        approvedBy:timesheet.approvedBy,

      }).then(res =>{
        this.$state.go('timesheets.info',{timesheetId:res.data._id},{reload:true});
      })
  }


  deleteTimesheet(timesheet) {
    console.log('deleting...');
    console.log(timesheet);
    this.$http.delete(`/api/timesheets/${timesheet._id}`);
    this.$state.go('timesheets.list');
  }

}
