import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  InfoComponent {
  $http;
  $state;
  Modal;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    // 'ngInject';
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  // fetchWeekDates(fromDate,toDate){
  //   console.log("fromDate :"+fromDate);
  //   this.weekDates = [];
  //   var fromDt = new Date(fromDate);
  //
  //   while(fromDt <= toDate) {
  //     console.log("fromDt :"+fromDt);
  //     this.weekDates.push(new Date(fromDt));
  //     fromDt.setDate(fromDt.getDate() + 1);
  //   }
  //
  //   console.log("this.weekDates :"+this.weekDates);
  // }


  // convertDate(date){
  //    this.newtimesheet.date = new Date(date);
  // }

  deleteTimesheet(timesheet) {
    var deleteConfirmationModal = this.modal.confirm.delete((timesheet) => {
    this.$http.delete('/api/timesheets/'+timesheet._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, timesheet.empId);
      this.$state.go('timesheets.list');
    }).catch(err => {
      console.log('timesheet not deleted');
    });
  })
  deleteConfirmationModal(timesheet.empId, timesheet);
  }

}
