import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  ListComponent {
  $http;
  $state;
  socket;
  $filter;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  // timesheetlist=[];
  fromDate;
  toDate;


  /*@ngInject*/
  constructor($http, socket, $state, $filter, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.$filter = $filter;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('/api/timesheets')
      .then(response => {
        this.timesheets = response.data;
      }).catch(err => {
  })
  }

  fetchTimesheet(fromDate,toDate) {
  this.$http.get('api/timesheets/')
  .then(response => {
    var filtered = [];
    var from_date = Date.parse(fromDate);
    var to_date = Date.parse(toDate);
    console.log(from_date);
    console.log(to_date);
    angular.forEach(response.data, function(item) {
      var fromdate = Date.parse(item.from);
      var todate = Date.parse(item.to);
         if(fromdate > from_date && todate < to_date) {
           console.log('hi'+item);
            filtered.push(item);
          }
        });
        console.log(filtered);
        this.timesheets = filtered;
      })
    }
    reset() {
      this.$http.get('api/holiday/')
      .then(response => {
        this.fromDate = null;
        this.toDate = null;
        this.timesheets= response.data;
      });
  }

  deleteTimesheet(timesheet) {
    this.$http.delete(`/api/timesheets/${timesheet._id}`);
  }
}
