import angular from 'angular';
import uiRouter from 'angular-ui-router';
import moment from 'moment';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newtimesheet = {
    empName :'',
    empId :'',
    weeknumber:'',
    year:'',
    location:'',
    from: '',
    to: '',
    entries : [{
      tsdate :'',
      manhours :0,
    }],

    total:0,

    approvedBy :'',

  };

  dateOptions={
    minDate: new Date()
  }
  today = new Date()
  message = 'Timesheet';
  weekDates =[];
  showdp = false;

  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

    today (){
      this.newtimesheet.to = new Date();
    };

    showcalendar($event) {
      this.showdp = true;
    };

    fetchWeekDates(fromDate,toDate){
      console.log("fromDate :"+fromDate);
      this.newtimesheet.entries = [];
      var fromDt = new Date(fromDate);

      while(fromDt <= toDate) {
        console.log("fromDt :"+fromDt);
        this.newtimesheet.entries.push({
          tsdate:new Date(fromDt),
          manhours:0
        });
        fromDt.setDate(fromDt.getDate() + 1);
      }

      console.log("this.weekDates :"+this.weekDates);
    }

    fetchWeekNumber(from, to){
      var date = moment(from).format('DD-MM-YYYY');
      // var date = moment(to).format('DD-MM-YYYY');
      var weeknum = moment(date, "DD-MM-YYYY").isoWeek();
      // var whichYear = moment().year();
      console.log('hi'+ weeknum);
      // console.log('hi'+ whichYear);
      this.newtimesheet.weeknumber=weeknum;
      // this.newtimesheet.year=whichYear;
    }




 timesheetCalculations(){
    this.newtimesheet.total = 0;
    var total = 0;
    this.newtimesheet.entries.forEach(function (arrayItem) {
      console.log(arrayItem)
      total += arrayItem.manhours;
  });
  this.newtimesheet.total = total;
//  return total;
 }

 disabled(currentDate, mode) {
 // console.log("sadsasdsda : "+mode);
return (mode === 'day' && (currentDate.getDay() === 0 || currentDate.getDay() === 6));
};

 //    toggleShow(Model){
 //    Model.showStartEvent = !Model.showStartEvent;
 // }
// timesheetCalculations(item){
//   this.newtimesheet.total=0;
//   console.log(item);
//   var total=0;
//   for (var property in item) {
//     if (item.hasOwnProperty(property)) {
//         // do stuff
//
//         this.newtimesheet.total += item[property];
//     }
// }
//   for(var i=0;i<this.manhours;i++){
//     total+=this.newtimesheet.manhours;
//   }
//   return total;
// }

  submitTimesheet(timesheetForm){
    if(timesheetForm.$invalid){
      this.submitted=true;
      return;
     }
    if(this.newtimesheet.empId) {
      this.$http.post('/api/timesheets', this.newtimesheet)
      .then( response => {
        //this.$state.go('pawns.info', {name: response.data.name});
        this.$state.go('timesheets.info', {timesheetId: response.data._id});
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }



  resetForm(){
    this.newtimesheet = {
      empName :'',
      empId :'',
      weeknumber:'',
      year:'',
      location:'',
      from: '',
      to: '',
      entries : [{
        tsdate :'',
        manhours :0,
      }],
      total:0,
      // submittedBy:'',
      // 5b8e11b81aa60f073b90902
      approvedBy :'',


    };
  }

}
