'use strict';

import {TimesheetComponent} from './timesheets/timesheets.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('timesheets', {
    url: '/timesheets',
    name: 'timesheets',
    abstract: true,
    authenticate: 'user',
    template: require('./timesheets/timesheets.html'),
    // component: 'pawns',
    controllerAs: '$ctrl',
    controller: TimesheetComponent,
  });

  $stateProvider.state('timesheets.list', {
    url: '/list',
    name: 'timesheets.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'pawns.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('timesheets.new', {
    url: '/new',
    name: 'timesheets.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'pawns.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('timesheets.info', {
    url: '/:timesheetId',
    name: 'timesheets.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {

      timesheetId: function($stateParams){
        return $stateParams.timesheetId;
      },
      timesheet: function($http, $stateParams){
        return $http.get('/api/timesheets/'+$stateParams.timesheetId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'pawns.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
  $stateProvider.state('timesheets.edit', {
    url: '/:timesheetId/edit',
    name: 'timesheets.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      timesheetId: function($stateParams){
        return $stateParams.timesheetId;
      },
      timesheet: function($http, $stateParams){
        return $http.get('/api/timesheets/'+$stateParams.timesheetId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'employees.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
