import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  InfoComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
 leaverequest;


  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth, Modal) {
    // 'ngInject';
    this.modal = Modal;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedIn;
    Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  convertDate(date){
     this.leaverequest.from = new Date(date)
      this.leaverequest.to
   }

  deleteleaverequest(leaverequest) {
    var deleteConfirmationModal = this.modal.confirm.delete((leaverequest) => {
    this.$http.delete('/api/leaverequests/'+leaverequest._id,{
    }).then(response=>{
      var modalAlert = this.modal.confirm.modalAlert(response, leaverequest.empname);
      this.$state.go('leaverequest.list');
    }).catch(err => {
      console.log('leaverequest not deleted');
    });
  })
  deleteConfirmationModal(leaverequest.empname, leaverequest);
  }

}
