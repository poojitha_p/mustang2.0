import angular from 'angular';
import uiRouter from 'angular-ui-router';

export class  EditComponent {
  $http;
  $state;
  $stateParams;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  leaverequest: {};

  /*@ngInject*/
  constructor($http, $stateParams, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }
  convertDate(date){
      this.leaverequest.from = new Date(date)

   }
   updateleaverequest(leaverequest){
       this.$http.put('api/leaverequests/'+leaverequest._id,{
         empname: leaverequest.empname,
         empId: leaverequest.empId,
         department: leaverequest.department,
         leavetype: leaverequest.leavetype,
         leavestatus: leaverequest.leavestatus,
         from: leaverequest.from,
         to: leaverequest.to,
         leavemessage: leaverequest.leavemessage,
         noofdays: leaverequest.noofdays,
         appprovedby: leaverequest.appprovedby
       }).then(res =>{
         this.$state.go('leaverequest.info',{leaverequestId:leaverequest._id},{reload:true});
       })
   }


  deleteleaverequest(leaverequest) {
    console.log('deleting...');
    console.log(leaverequest);
    this.$http.delete(`/api/leaverequests/${leaverequest._id}`);
    this.$state.go('leaverequest.list');
  }

}
