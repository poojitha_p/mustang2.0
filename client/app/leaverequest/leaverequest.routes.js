'use strict';

import {LeaverequestComponent} from './leaverequest/leaverequest.component';
import {ListComponent} from './list/list.component';
import {NewComponent} from './new/new.component';
import {InfoComponent} from './info/info.component';
import {EditComponent} from './edit/edit.component';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('leaverequest', {
    url: '/leaverequest',
    name: 'leaverequest',
    abstract: true,
    authenticate: 'user',
    template: require('./leaverequest/leaverequest.html'),
    // component: 'leaverequest',
    controllerAs: '$ctrl',
    controller: LeaverequestComponent,
  });

  $stateProvider.state('leaverequest.list', {
    url: '/list',
    name: 'leaverequest.list',
    authenticate: 'user',
    template: require('./list/list.html'),
    // component: 'leaverequest.list',
    controllerAs: '$ctrl',
    controller: ListComponent,
  });

  $stateProvider.state('leaverequest.new', {
    url: '/new',
    name: 'leaverequest.new',
    authenticate: 'user',
    template: require('./new/new.html'),
    // component: 'leaverequest.new',
    controllerAs: '$ctrl',
    controller: NewComponent,
  });

  $stateProvider.state('leaverequest.info', {
    url: '/:leaverequestId',
    name: 'leaverequest.info',
    authenticate: 'user',
    template: require('./info/info.html'),
    resolve: {

      leaverequestId: function($stateParams){
        return $stateParams.leaverequestId;
      },
      leaverequest: function($http, $stateParams){
        return $http.get('/api/leaverequests/'+$stateParams.leaverequestId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'leaverequest.info',
    controllerAs: '$ctrl',
    controller: InfoComponent,
  });
  $stateProvider.state('leaverequest.edit', {
    url: '/:leaverequestId/edit',
    name: 'leaverequest.edit',
    authenticate: 'user',
    template: require('./edit/edit.html'),
    resolve: {
      leaverequestId: function($stateParams){
        return $stateParams.leaverequestId;
      },
      leaverequest: function($http, $stateParams){
        return $http.get('/api/leaverequests/'+$stateParams.leaverequestId)
          .then(response => {
            return response.data;
          });
      },
    },
    // component: 'leaverequest.info',
    controllerAs: '$ctrl',
    controller: EditComponent,
  });
}
