'use strict';

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './leaverequest.routes';

export default angular.module('mustangApp.leaverequest', [uiRouter])
  .config(routing)
  .name;
