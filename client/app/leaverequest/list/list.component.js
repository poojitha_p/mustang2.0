import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  ListComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  leaverequest=[];

  /*@ngInject*/
  constructor($http, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
    this.$http.get('/api/leaverequests')
      .then(response => {
        this.leaverequest = response.data;
      });
  }

  deleteleaverequest(leaverequest) {
    this.$http.delete(`/api/leaverequests/${leaverequest._id}`);
  }
}
