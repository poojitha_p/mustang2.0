import angular from 'angular';
import uiRouter from 'angular-ui-router';
// import routing from './dashboard.routes';

export class  NewComponent {
  $http;
  $state;
  socket;
  isLoggedIn: Function;
  isAdmin: Function;
  getCurrentUser: Function;
  newleaverequest = {
    empname: '',
    empId: '5b8e11b81aa60f073b90902d',
    department: '',
    leavetype: '',
    employeestatus: '',
    leavestatus: '',
    from: '',
    to: '',
    leavemessage: '',
    leaveAccrueEnabled: '',
    leaveCarriedForwared: '',
    leavePerYear: '',
    noofdays: '',
    appprovedby:'',
  //  leavetype= {
  //   Vacationleaves: '',
  //   Annualleaves: '',
  //   Sickleaves: '',
  //   Casualleaves: '',
  //   Earnedleaves/pl: '',
  //   Maternityleaves: '',
  //   Paternityleaves: '',
  //   Transferleaves: '',
  //
  // };
  };
  message = 'Add a new leaverequest from here.';
  department = ['IT','HR Recutiment','Research & Development','Business Development','Supportive Role','Finance','Sales & Marketing'];
  leavetype = ['Vacation','Annual Leave','Sick leave','Casual Leave','Earned Leave/PL','Maternity Leave','Paternity Leave','Transfer Leave'];
  employeestatus =['Full-Time Employees','Part-Time Employees','Temporary Employees','contract employees'];
  leavestatus = ['Approved','Pending','Avalilable','Rejected']
  /*@ngInject*/
  constructor($http, $scope, socket, $state, Auth) {
    // 'ngInject';
    this.$http = $http;
    this.$state = $state;
    this.socket = socket;
    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }

  $onInit() {
  }

  addleaverequest(myForm) {
  if(this.newleaverequest.empname) {
        this.$http.post('/api/leaverequests', this.newleaverequest)
      // console.log(hi)
      .then( response => {

        //this.$state.go('leaverequests.info', {name: response.data.name});
        this.$state.go('leaverequest.info', {leaverequestId: response.data._id});
        this.resetForm();
      }).catch( err => {
        console.log('oops');
      });

    }
  }
  disabled(currentDate, mode) {
  return (mode === 'day' && (currentDate.getDay() === 0 || currentDate.getDay() === 6));
  };


  dateDifference(from, to) {
    if(from && to){
    var fromdt = new Date(from);
    var todt = new Date(to);
    var diff = Math.round((todt -fromdt )/1000/60/60/24);
    this.newleaverequest.noofdays = diff;
  }else{
  this.newleaverequest.noofdays ='';
   }
  }


   // dateDifference(from,to) {
   //  var fromdt = $("#fromdt").data("DatePicker").value(fromdt);
   //  var todt = $("#todt").data("DatePicker").value(todt);
   //
   //  var diff = Math.round((todt - startDate)/1000/60/60/24); //Difference in days
   // }

  resetForm(){
    this.newleaverequest = {
      empname: '',
      empId: '' ,
      department: '',
      leavetype: '',
      employeestatus: '',
      from: '',
      to: '',
      leavemessage: '',
      leaveAccrueEnabled: '',
      leaveCarriedForwared: '',
      leavePerYear: '',
      noofdays:'',
      appprovedby: '',

    };
  }

}
